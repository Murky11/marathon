@echo off

set defines=-DBUILD_DEBUG=1
set opts=-FC -GR- -EHa- -nologo
set debug=-Zi -W3 
set ignored=-wd4244 -wd4312 -wd4267 -wd4996
set code=%cd%\code
REM Thought this would be included with sokol??
set libs=opengl32.lib

if not exist build mkdir build
pushd build
cl %defines% %opts% %debug% %ignored% %code%\marathon.cpp %libs% -FeMarathon
xcopy ..\assets .\assets /E /I /Q /Y
popd
