#ifndef RENDERER_H
#define RENDERER_H

// Let's use Microui types for convinience
function void RendererInit(void);
function void RendererDrawRect(mu_Rect rect, mu_Color color);
function void RendererDrawText(String8 text, mu_Vec2 pos, mu_Color color);
function u32  RendererGetTextWidth(String8 text);
function u32  RendererGetTextHeight(void);
function void RendererSetClipRect(mu_Rect rect);
function void RendererClear(mu_Color color);
function void RendererPresent(void);

#endif //RENDERER_H
