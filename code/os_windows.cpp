#include <windows.h>

//~ Scratch arenas

// Will I ever pass more than one arena to any function at a time?
function TempArena
OSGetScratch(Arena *pConflict)
{
    // TODO: Actually implement good thread context handling
    local_persist thread_storage Arena *arenas[2];
    if (arenas[0] == 0) {
        arenas[0] = ArenaAlloc(ArenaFlag_MMU);
        arenas[1] = ArenaAlloc(ArenaFlag_MMU);
    }
    Arena *result = (pConflict == arenas[0]) ? arenas[1] : arenas[0];
    
    return ArenaTempBegin(result);
}

//~ Memory management

function void* 
OSMemReserve(u64 size)
{
    return VirtualAlloc(0, size, MEM_RESERVE, PAGE_READWRITE);
}

function void  
OSMemCommit(void *ptr, u64 size)
{
    VirtualAlloc(ptr, size, MEM_COMMIT, PAGE_READWRITE);
}

function void  
OSMemDecommit(void *ptr, u64 size)
{
    VirtualFree(ptr, size, MEM_DECOMMIT);
}

function void  
OSMemRelease(void *ptr, u64 size)
{
    VirtualFree(ptr, size, MEM_RELEASE);
}

//~ IO

function void
OSDebugLog_Impl(String8 msg)
{
    OutputDebugString((LPCSTR)msg.str);
}

function void*
OSReadEntireFile(Arena *arena, String8 fileName)
{
    void *result = 0;
    
    HANDLE file = CreateFile((LPCSTR)fileName.str, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
    if (file != INVALID_HANDLE_VALUE) {
        LARGE_INTEGER fileSize;
        GetFileSizeEx(file, &fileSize);
        Assert(fileSize.QuadPart <= u32_max);
        u64 backupPos = ArenaGetPos(arena);
        result = ArenaPush(arena, fileSize.QuadPart, ARENA_ALIGNMENT_UNSPECIFIED, false);
        if (result) {
            s32 bytesRead;
            if (ReadFile(file, result, fileSize.QuadPart, (LPDWORD)&bytesRead, 0) && bytesRead == fileSize.QuadPart) {
            } else {
                ArenaPopTo(arena, backupPos);
            }
        }
    }
    
    return result;
}