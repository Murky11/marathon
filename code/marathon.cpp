//~ Headers

#define RENDERER_OPENGL

//- Codebase
#define MemImpl_Reserve(size) OSMemReserve(size)
#define MemImpl_Commit(ptr, size) OSMemCommit(ptr, size)
#define MemImpl_Decommit(ptr, size) OSMemDecommit(ptr, size)
#define MemImpl_Release(ptr, size) OSMemRelease(ptr, size)

#include "base/base_include.h"

//- Microui
#include "third_party/microui/microui.h"

//- OS
#include "os.h"

//- Renderer
#include "renderer.h"

//~ Source

//- Codebase
#include "base/base_include.c"

//- Mircoui
#include "third_party/microui/microui.c"

//- Sokol (technically a source file)
#undef function
#undef global
#undef local_persist

#define SOKOL_ASSERT(c) Assert(c)
#define SOKOL_UNREACHABLE InvalidPath()

#if BUILD_DEBUG
# define SOKOL_DEBUG
#endif

#ifdef RENDERER_OPENGL
# define SOKOL_GLCORE33
#endif
#define SOKOL_IMPL
#include "third_party/sokol/sokol_app.h"

#define function static
#define global static
#define local_persist static

//- OS
#if OS_WINDOWS
# include "os_windows.cpp"
#else
# error Unimplemented OS backend!
#endif

//- stb_truetype
#define STB_TRUETYPE_IMPLEMENTATION
#define STBTT_STATIC
#include "third_party/stb/stb_truetype.h"

//- Renderer
#ifdef RENDERER_OPENGL
# include "gl/gl.h" // Must be included after windows.h (included in sokol)
# include "renderer_opengl.cpp"
#endif

//global mu_Context muiCtx;

function void
AppInit(void)
{
    Arena *permanentArena = ArenaAlloc(ArenaFlag_Chained | ArenaFlag_MMU);
    
    
    //- Init Microui
    /*mu_init(&muiCtx);
    muiCtx->text_width = ;
    muiCtx->text_height = ;*/
}

function void
AppFrame(void)
{
    
}

function void
AppEvent(const sapp_event *event)
{
    UnusedVariable(event);
}

function void
AppCleanup(void)
{
}

sapp_desc
sokol_main(int argc, char **argv)
{
    UnusedVariable(argc);
    UnusedVariable(argv);
    
    sapp_desc result = {0};
    result.high_dpi = true;
    result.window_title = "Marathon";
    result.enable_clipboard = true;
    result.enable_dragndrop = true;
    
    result.init_cb = AppInit;
    result.frame_cb = AppFrame;
    result.event_cb = AppEvent;
    result.cleanup_cb = AppCleanup;
    
    return result;
}