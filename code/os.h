#ifndef OS_H
#define OS_H

//~ Scratch arenas

function TempArena OSGetScratch(Arena *pConflict);
#define OSReleaseScratch(scratch) ArenaTempEnd(scratch)

//~ Memory management

function void* OSMemReserve(u64 size);
function void  OSMemCommit(void *ptr, u64 size);
function void  OSMemDecommit(void *ptr, u64 size);
function void  OSMemRelease(void *ptr, u64 size);

//~ IO

function void
OSDebugLog_Impl(String8 msg);
#if BUILD_DEBUG
# define OSDebugLog(msg) OSDebugLog_Impl(msg)
#else
# define OSDebugLog(msg)
#endif

function void*
OSReadEntireFile(Arena *arena, String8 fileName);

#endif //OS_H
