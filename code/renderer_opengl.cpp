typedef struct GlState GlState;
struct GlState
{
    int placeholder;
};

function void
RendererInit(void)
{
    
}

function void
RendererFlush_OpenGL(void)
{
    
}

function void
RendererClear(mu_Color color)
{
    glClearColor(color.r / 255.f, color.g / 255.f, color.b / 255.f, color.a / 255.f);
    glClear(GL_COLOR_BUFFER_BIT);
}