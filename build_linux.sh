#!/bin/bash

code="$PWD"
opts=-g
cd build > /dev/null
gcc $opts $code/marathon.c -o Marathon
cd $code > /dev/null
