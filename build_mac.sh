#!/bin/bash

code="$PWD/code"
opts=-g
apple_frameworks="-framework Cocoa -framework QuartzCore -framework Metal -framework Metalkit
cd build > /dev/null
clang $opts $code/marathon.c -o Marathon
cd $code > /dev/null
